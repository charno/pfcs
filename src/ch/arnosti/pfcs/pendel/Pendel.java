package ch.arnosti.pfcs.pendel;


import javax.media.opengl.*;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.media.opengl.awt.GLCanvas;
import javax.swing.JFrame;
import javax.swing.JLabel;

import ch.arnosti.pfcs.utils.Utilities;

import com.jogamp.opengl.util.FPSAnimator;

public class Pendel implements GLEventListener, KeyListener {

	private double height;
	private double width =  20;
	
	// Physical variables
	private double y = 0; // Center of circle
	private double x = -4;

	private double xb;
	private double yb;

	private int step = 0;
	private int steps = 200;

	double r = 0.5; // r of the ball
	double r2 = 2; // r of the circle the ball is evolving around

	// Other variables
	private boolean drawCircle = false;

	public Pendel() {
		// Initialize the window
		JFrame f = new JFrame("Pendel");
		f.setLayout(new BorderLayout());
		f.setSize(800, 600);
		GLCanvas canvas = new GLCanvas();
		canvas.addGLEventListener(this);
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.add(canvas, BorderLayout.CENTER);
		//f.addKeyListener(this);
		canvas.addKeyListener(this);
		
		JLabel man = new JLabel();
		man.setText("<html>In dieser Übung kann die Darstellung folgendermassen gesteuert werden:<br/>- r: Darstellen des Rotors auf der Kreisbahn<br/>- Hoch / runter: Beschleunigen der Pendelbewegung<br/>- a / A: Amplitude des Pendels verkleinern bzw. vergrössern</html>");
		f.add(man, BorderLayout.SOUTH);
		
		f.setVisible(true);

		// Animator
		FPSAnimator animator = new FPSAnimator(canvas, 60, true);
		animator.start();

	}

	// Physical calculations

	// Calculate x and y of a point evolving around another x and y
	void calculateXY() {
		double dt = 2.0 * Math.PI / steps;
		this.xb = r2 * Math.cos(step * dt) + x;
		this.yb = r2 * Math.sin(step * dt) + y;
	}

	// GL methods

	/**
	 * Zeichne einen Ball um (xm, ym) mit Radius r
	 */
	void drawBall(GL2 gl, double r, double xm, double ym, boolean filled) {
		int nPkte = 40; // Anzahl Punkte
		double dt = 2.0 * Math.PI / nPkte; // Parameter-Schrittweite
		gl.glBegin(filled ? GL2.GL_POLYGON : GL2.GL_LINE_LOOP);
		for (int i = 0; i < nPkte; i++) {
			gl.glVertex2d(r * Math.cos(i * dt) + xm, // x = r*cos(i*dt) + xm
					r * Math.sin(i * dt) + ym); // y = r*sin(i*dt-phi) + ym
		}
		gl.glEnd();
	}

	// For best result use elements dividable by 3
	void drawVerticalSpiral(GL2 gl, int elements, double x, double halfwidth,
			double topy, double bottomy) {
		gl.glBegin(GL2.GL_LINE_STRIP);
		int xrelative = 1;
		for (int i = 0; i < elements; i++) {
			gl.glVertex2d(x - halfwidth + halfwidth * xrelative,
					((topy - bottomy) / elements) * i + bottomy);
			xrelative = (xrelative + 1) % 3;
		}
		gl.glEnd();
	}

	// --------- OpenGL-Events -----------------------

	public void init(GLAutoDrawable drawable) {
		GL gl0 = drawable.getGL(); // OpenGL-Objekt
		GL2 gl = gl0.getGL2();
		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // erasing color
	}

	public void display(GLAutoDrawable drawable) {
		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();

		Utilities.drawBackground(gl, height, width);

		// refresh coordinates
		calculateXY();

		gl.glColor3f(1.0f, 0.0f, 0.0f);
		drawBall(gl, r, 3, yb, true); // Draw the featherpendulum

		gl.glColor3f(1.0f, 1.0f, 1.0f);
		drawVerticalSpiral(gl, 22, 3, 0.5, 6, yb + r); // Draw the feather

		if (drawCircle) {
			gl.glColor3f(1.0f, 0.0f, 0.0f);

			drawBall(gl, r, xb, yb, true); // Draw the circle used for the
										// featherpendulum
			gl.glColor3f(1.0f, 1.0f, 1.0f);

			gl.glBegin(GL2.GL_LINES);
			gl.glVertex2d(xb, yb + r);
			gl.glVertex2d(3, yb + r);
			gl.glVertex2d(xb, yb - r);
			gl.glVertex2d(3, yb - r);
			gl.glEnd();
			
			drawBall(gl, r2, x, y, false);

		}

		// Change the step
		step = (step + 1) % steps;

	}
	public void reshape(GLAutoDrawable drawable, // Window resized
			int x, int y, int width, int height) {
		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();
		gl.glViewport(0, 0, width, height); // Window
		double aspect = (double) height / width;
		double left = -(this.width / 2), right = (this.width / 2);
		this.height = this.width * aspect;
		double bottom = (-this.height / 2), top = (this.height / 2);
		double near = -100, far = 100;
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrtho(left, right, bottom, top, near, far); // ViewingVolume
	}

	public void dispose(GLAutoDrawable drawable) {
	}

	// -----------------------------------------------------------------------
	// Keyboard events
	// -----------------------------------------------------------------------

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		// Switch the DrawCircle
		case KeyEvent.VK_R:
			drawCircle = !drawCircle;
			e.consume();
			break;
		case KeyEvent.VK_DOWN:
			steps++;
			e.consume();
			break;
		case KeyEvent.VK_UP:
			steps--;
			if (steps < 10) {
				steps = 10;
			}
			e.consume();
			break;
		case KeyEvent.VK_A:
			if (e.isShiftDown()) {
				r2 += 0.1;
				if (r2 > 5.0) {
					r2 = 5.0;
				}
			} else {
				r2 -= 0.1;
				if (r2 < 0.5) {
					r2 = 0.5;
				}
			}
			e.consume();
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}
}
