package ch.arnosti.pfcs.tennisball;

import javax.media.opengl.*;

import java.awt.*;
import java.awt.event.*;
import java.util.Iterator;
import java.util.LinkedList;

import javax.media.opengl.awt.GLCanvas;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ch.arnosti.pfcs.utils.Utilities;

import com.jogamp.opengl.util.*;
import com.jogamp.opengl.util.gl2.GLUT;

public class Tennisball implements GLEventListener, KeyListener, 
		DrawCurveListener {

	// OpenGL variables
	private double height; // calculated
	private double width = 20f; // m
	private double rotatey = 0;
	private double rotatex = 0;
	private int fps = 60;

	// Physical variables
	private double startx = -9f; // m
	private double starty = 1.8f; // m

	private double alpha = 45; // °

	private double v = 10; // start speed, m/s

	private double dt = 1.0 / fps; // s

	// Shooting variables
	private int counter = 0;

	private int shootAllXFrames = fps;

	// display variables
	private boolean drawCalculated = false;

	// Helper
	private java.util.List<Ball> balls = new LinkedList<Ball>();

	// GUI
	JCheckBox chbDrawCurve = new JCheckBox("Draw Curve without air resistance",
			drawCalculated);

	JSlider sldForce = new JSlider(0, 25, getForce());
	JSlider sldAngle = new JSlider(-90, 90, getAngle());

	public Tennisball() // Konstruktor
	{
		JFrame f = new JFrame("Tennisball (Mit Luftwiderstand)");
		f.setLayout(new BorderLayout());
		f.setSize(800, 600);

		GLCanvas canvas = new GLCanvas(); // OpenGL-Window
		canvas.addGLEventListener(this);

		// Add Canvas
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.add(canvas, BorderLayout.CENTER);

		// Add settings pane
		JPanel settingsPanel = new JPanel();
		settingsPanel.setLayout(new FlowLayout());

		// Add curvedarwing?
		settingsPanel.add(chbDrawCurve);
		chbDrawCurve.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (chbDrawCurve.isSelected() != drawCalculated) {
					toggleDrawCurve();
				}
			}
		});

		// Add Force slider
		sldForce.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				setForce(sldForce.getValue());
			}
		});

		sldForce.setMajorTickSpacing(10);
		sldForce.setMinorTickSpacing(1);
		sldForce.setPaintTicks(true);
		sldForce.setPaintLabels(true);

		JPanel forcePanel = new JPanel();
		forcePanel.setLayout(new BoxLayout(forcePanel, BoxLayout.PAGE_AXIS));

		forcePanel.add(new JLabel("Abschussgeschwindigkeit"));
		forcePanel.add(sldForce);

		settingsPanel.add(forcePanel);

		
		// Add Angle slider
		sldAngle.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				setAngle(sldAngle.getValue());
			}
		});

		sldAngle.setMajorTickSpacing(90);
		sldAngle.setMinorTickSpacing(15);
		sldAngle.setPaintTicks(true);
		sldAngle.setPaintLabels(true);

		
		
		JPanel anglePanel = new JPanel();
		anglePanel.setLayout(new BoxLayout(anglePanel, BoxLayout.PAGE_AXIS));

		anglePanel.add(new JLabel("Abschusswinkel"));
		anglePanel.add(sldAngle);

		settingsPanel.add(anglePanel);
		
		
		f.add(settingsPanel, BorderLayout.SOUTH);

		f.setVisible(true);
		canvas.addKeyListener(this);

		FPSAnimator anim = new FPSAnimator(canvas, fps, true);
		anim.start();
	}

	// --------- OpenGL-Events -----------------------

	public void init(GLAutoDrawable drawable) {
		GL gl0 = drawable.getGL(); // OpenGL-Objekt
		GL2 gl = gl0.getGL2();
		gl.glEnable(GL2.GL_POINT_SMOOTH);
		gl.glEnable(GL2.GL_LINE_SMOOTH);

	}

	/**
	 * @author Dario Todaro
	 */
	private void zeichneKurve(GL2 gl) {
		gl.glBegin(GL2.GL_LINE_STRIP);
		for (double x = startx; x < 10; x += 0.1) {
			double y = (Math.tan(Math.toRadians(alpha)) * (x - startx))
					- ((9.81 / (2 * v * v * Math.cos(Math.toRadians(alpha)) * Math
							.cos(Math.toRadians(alpha)))) * ((x - startx) * (x - startx)))
					+ starty;
			gl.glVertex2d(x, y);
		}
		gl.glEnd();
	}

	public void display(GLAutoDrawable drawable) {
		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
		gl.glRotated(rotatey, 0, 1, 0);
		gl.glRotated(rotatex, 1, 0, 0);

		Utilities.drawBackground(gl, height, width);

		counter = (counter + 1) % shootAllXFrames;

		// Every second a new ball
		if (counter == 0) {
			balls.add(new Ball(startx, starty, alpha, v));
		}

		gl.glColor3d(0.5, 0.0, 0.0);

		if (drawCalculated) {
			zeichneKurve(gl);
		}
		gl.glColor3d(1.0, 1.0, 1.0);

		// Draw the balls
		Iterator<Ball> it = balls.iterator();
		while (it.hasNext()) {
			Ball ball = it.next();
			ball.step(dt);
			ball.draw(gl);
			if (ball.isOudside()) {
				it.remove();
			}
		}


	}

	public void reshape(GLAutoDrawable drawable, // Window resized
			int x, int y, int width, int height) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glViewport(0, 0, width, height); // Window
		double aspect = (double) height / width;
		double left = -(this.width / 2), right = (this.width / 2);
		this.height = this.width * aspect;
		double bottom = (-this.height / 2), top = (this.height / 2);
		double near = -100, far = 100;
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrtho(left, right, bottom, top, near, far); // ViewingVolume
	}

	public void dispose(GLAutoDrawable drawable) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case (KeyEvent.VK_UP):
			setAngle(getAngle() + 1);
			break;
		case (KeyEvent.VK_DOWN):
			setAngle(getAngle() - 1);
			break;
		case (KeyEvent.VK_LEFT):
			setForce(getForce() - 1);
			break;
		case (KeyEvent.VK_RIGHT):
			setForce(getForce() + 1);
			break;

		/*
		 * case (KeyEvent.VK_W): rotatex--;
		 * 
		 * break; case (KeyEvent.VK_A): rotatey++; break; case (KeyEvent.VK_S):
		 * rotatex++; break; case (KeyEvent.VK_D): rotatey--; break;
		 */
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	private class Ball {

		// coordinates
		double x, y; // x, y: m ; alpha: radians; v: m/s

		// speed
		double vx, vy; // m/s

		// ball
		double r = 0.034; // m
		double m = 0.058; // g
		double cW = 0.4; // Widerstandswert

		// constants
		double g = 9.81; // m/s^2

		public Ball(double x, double y, double alpha, double v) {
			this.x = x;
			this.y = y;
			this.vx = v * Math.cos(2 * Math.PI * alpha / 360);
			this.vy = v * Math.sin(2 * Math.PI * alpha / 360);
		}

		public void step(double sec) {

			// iterative point
			x += vx * sec;
			y += vy * sec;

			double v = Math.sqrt(vx * vx + vy * vy);

			double c = 1.2929 / 2 * cW * r * r * Math.PI;

			double Rx = -c * v * vx;
			double Ry = -c * v * vy;

			double Fx = Rx;
			double Fy = Ry - m * g;

			double ax = Fx / m;
			double ay = Fy / m;

			vx += ax * sec;
			vy += ay * sec;

		}

		public void draw(GL2 gl) {
			gl.glPushMatrix();
			gl.glTranslated(x, y, 0);
			GLUT glut = new GLUT();
			glut.glutSolidSphere(r, 10, 10);
			gl.glPopMatrix();
		}

		public boolean isOudside() {
			return y < -20.0;
		}

	}

	// Listeners

	@Override
	public void toggleDrawCurve() {
		drawCalculated = !drawCalculated;
		// update GUI

	}

	@Override
	public void isDrawCurve() {
		// TODO Auto-generated method stub

	}

	public void setForce(int force) {
		if (force != getForce()) {
			if (force < sldForce.getMinimum()) {
				v = sldForce.getMinimum();
			} else if (force > sldForce.getMaximum()) {
				v = sldForce.getMaximum();
			} else {
				v = force;
			}
			sldForce.setValue(getForce());
		}
	}

	public int getForce() {
		return (int) v;
	}

	public void setAngle(int angle) {
		if (angle != getAngle()) {
			if (angle < sldAngle.getMinimum()) {
				alpha = sldAngle.getMinimum();
			} else if (angle > sldAngle.getMaximum()) {
				alpha = sldAngle.getMaximum();
			} else {
				alpha = angle;
			}
			sldAngle.setValue(getAngle());
		}
	}

	public int getAngle() {
		// TODO Auto-generated method stub
		return (int) alpha;
	}

}