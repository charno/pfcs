package ch.arnosti.pfcs.schieferwurf;

import javax.media.opengl.*;

import java.awt.*;
import java.awt.event.*;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JFrame;

import ch.arnosti.pfcs.utils.Utilities;

import com.jogamp.opengl.util.*;

public class SchieferWurf implements GLEventListener {

	private double height;
	private double width = 20f;

	double xm = -6;
	double ym = 1.8; // Koordinaten Ball
	double vx = 10; // m/s
	double vy = 10; // m/s
	double dt = 0.01; // s
	double r = 0.1; // m
	final double g = 9.81; // Erdbeschleunigung m/s^2
	double ay = -g;

	// ------------------ Methoden --------------------



	void zeichneKreis(GL2 gl, double r, double xm, double ym) // Kreis um den
																// Nullpunkt
	{
		int nPkte = 40; // Anzahl Punkte
		double dt = 2.0 * Math.PI / nPkte; // Parameter-Schrittweite
		gl.glBegin(gl.GL_POLYGON);
		for (int i = 0; i < nPkte; i++)
			gl.glVertex2d(xm + r * Math.cos(i * dt), // x = r*cos(i*dt)
					ym + r * Math.sin(i * dt)); // y = r*sin(i*dt-phi)
		gl.glEnd();
	}

	public SchieferWurf() // Konstruktor
	{
		JFrame f = new JFrame("Schiefer Wurf");
		f.setSize(800, 600);
		GLCanvas canvas = new GLCanvas(); // OpenGL-Window
		canvas.addGLEventListener(this);
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.add(canvas);
		f.setVisible(true);

		FPSAnimator anim = new FPSAnimator(canvas, 200, true);
		anim.start();
	}

	// --------- OpenGL-Events -----------------------

	public void init(GLAutoDrawable drawable) {
		GL gl0 = drawable.getGL(); // OpenGL-Objekt
		GL2 gl = gl0.getGL2();
	}

	public void display(GLAutoDrawable drawable) {
		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();
		
		Utilities.drawBackground(gl, height, width);
		
		gl.glColor3d(1, 1, 1); // Zeichenfarbe
		zeichneKreis(gl, r, xm, ym);
		if (ym <= r) {
			vy = -vy;
			vy += ay * dt;
		}
		ym += vy * dt;
		vy += ay * dt;
		xm += vx * dt;
		if (xm >= (width / 2) - r) {
			vx *= -1;
		}
		if (xm <= -(width / 2) + r) {
			vx *= -1;
		}
	}

	public void reshape(GLAutoDrawable drawable, // Window resized
			int x, int y, int width, int height) {
		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();
		gl.glViewport(0, 0, width, height); // Window
		double aspect = (double) height / width;
		double left = -(this.width / 2), right = (this.width / 2);
		this.height = this.width * aspect;
		double bottom = (-this.height / 2), top = (this.height / 2);
		double near = -100, far = 100;
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrtho(left, right, bottom, top, near, far); // ViewingVolume
	}

	public void dispose(GLAutoDrawable drawable) {
	}

}