package ch.arnosti.pfcs.gyroskop;

import javax.media.opengl.*;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.fixedfunc.GLLightingFunc;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import javax.swing.JLabel;

import ch.arnosti.pfcs.utils.Utilities;

import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.gl2.GLUT;

public class Gyroskop implements GLEventListener, KeyListener {
	// Windowsize
	private double height;
	private double width = 20;

	// GLUT
	private GLUT glut;
	private GLU glu;

	public Gyroskop() {
		// Initialize the window
		JFrame f = new JFrame("Gyroskop");
		f.setLayout(new BorderLayout());
		f.setSize(800, 600);
		GLCanvas canvas = new GLCanvas();
		canvas.addGLEventListener(this);
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.add(canvas, BorderLayout.CENTER);
		// f.addKeyListener(this);
		canvas.addKeyListener(this);

		JLabel man = new JLabel();// TODO
		man.setText("<html>In dieser Übung kann die Darstellung folgendermassen gesteuert werden:<br/>- r: Darstellen des Rotors auf der Kreisbahn<br/>- Hoch / runter: Beschleunigen der Pendelbewegung<br/>- a / A: Amplitude des Pendels verkleinern bzw. vergrössern</html>");
		f.add(man, BorderLayout.SOUTH);

		f.setVisible(true);

		// Animator
		FPSAnimator animator = new FPSAnimator(canvas, 60, true);
		animator.start();
	}

	// --------- OpenGL-Events -----------------------

	public void init(GLAutoDrawable drawable) {
		GL gl0 = drawable.getGL(); // OpenGL-Objekt
		GL2 gl = gl0.getGL2();
		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // erasing color
        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glEnable(GLLightingFunc.GL_NORMALIZE);

        gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST); // best perspective correction
        gl.glEnable (GL2.GL_POLYGON_SMOOTH); 
        gl.glHint(GL2.GL_POLYGON_SMOOTH_HINT, GL.GL_NICEST); 
        gl.glHint(GL2ES1.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST); 
        gl.glShadeModel(GL2.GL_SMOOTH); // blends colors nicely, and smoothes out lighting
		glut = new GLUT();

		glu = new GLU();

	}

	public void enableLight(GL2 gl) {
		gl.glEnable(GL2.GL_LIGHTING);
		gl.glEnable(GL2.GL_LIGHT0);
		gl.glEnable(GL2.GL_LIGHT1);
		gl.glEnable(GL2.GL_LIGHT2);
		gl.glEnable(GLLightingFunc.GL_COLOR_MATERIAL);
	}

	public void disableLight(GL2 gl) {
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glDisable(GL2.GL_LIGHT0);
		gl.glDisable(GL2.GL_LIGHT1);
		gl.glDisable(GL2.GL_LIGHT2);
		gl.glDisable(GLLightingFunc.GL_COLOR_MATERIAL);

	}

	public void display(GLAutoDrawable drawable) {
		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		gl.glLoadIdentity();

	
		
		glu.gluLookAt(20, 20, 20, 0, 0, 0, 0, 1, 0);
		
		// disableLight(gl);

		Utilities.zeichneAchsen(gl, 60);

		// enableLight(gl);

		// Fuss
		gl.glPushMatrix();
		gl.glRotated(-90, 1, 0, 0);
		gl.glColor3d(1, 0, 0);
		glut.glutSolidCylinder(1, 0.5, 50, 1);
		gl.glColor3d(1, 1, 0);

		glut.glutSolidCylinder(0.1, 5, 50, 1);
		gl.glPopMatrix();

	}

	public void reshape(GLAutoDrawable drawable, // Window resized
			int x, int y, int width, int height) {
		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();
		gl.glViewport(0, 0, width, height); // Window
		double aspect = (double) height / width;
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluPerspective(45, aspect, 0.1, 100);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	public void dispose(GLAutoDrawable drawable) {
	}

	// -----------------------------------------------------------------------
	// Keyboard events
	// -----------------------------------------------------------------------

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}
}
