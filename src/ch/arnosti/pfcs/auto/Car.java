package ch.arnosti.pfcs.auto;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

public class Car {

	private double x = 0, y = 0; // m, Koordinaten hinters linkes Rad

	private double drehung = 0; // ° Drehung des Autos relativ zur X-Achse

	private double alpha = 0; // ° Drehung des linken Vorderrad relativ zum Auto

	private double beta = 0; // ° Drehung des rechten Vorderrad relativ zum Auto

	private double maxAlpha = 35; // ° Maximale Auslenkung, auch für beta

	private double v = 0; // m/s, Geschwindigkeit

	private double maxv = 100; // m/s, Maximale Geschwindigkeit, auch rückwärts

	double mx, my; // Koordinaten des Auto

	private static double halbeAchsenbreite = 0.9; // m

	private static double achsenabstand = 2.5; // m

	public double getYM() {
		return halbeAchsenbreite
				+ (achsenabstand / Math.tan(Math.toRadians(alpha)));
	}

	public void turnLeft() {
		if (alpha < maxAlpha && alpha >= 0) {
			alpha++;
			beta = Math.toDegrees(Math.atan(achsenabstand
					/ (getYM() + halbeAchsenbreite)));
		} else if (alpha < maxAlpha) {
			beta++;
			alpha = Math.toDegrees(Math.atan(achsenabstand
					/ ((-halbeAchsenbreite + (achsenabstand / Math.tan(Math // = ym
							.toRadians(beta)))) // = ym
					- halbeAchsenbreite)));
		}
	}

	public void turnRight() {
		if (beta > -maxAlpha && alpha >= 0.5) {
			alpha--;
			beta = Math.toDegrees(Math.atan(achsenabstand
					/ (getYM() + halbeAchsenbreite)));
		} else if (beta > -maxAlpha) {
			beta--;
			alpha = Math.toDegrees(Math.atan(achsenabstand
					/ ((-halbeAchsenbreite + (achsenabstand / Math.tan(Math // = ym
							.toRadians(beta)))) // = ym
					- halbeAchsenbreite)));
		}
	}

	public void goFaster() {
		if (v < maxv) {
			v++;
		}
	}

	public void goSlower() {
		if (v > -maxv) {
			v--;
		}
	}

	public double getZentripetal() {
		return Math.abs(((double) v * v) / getYM());
	}

	public void calculateStep(double sec) {

		if (alpha != 0) {

			// Abstand von M zum linken Hinterrad
			double c = getYM() - halbeAchsenbreite;

			// Distance on circle
			double distanz = v * sec;

			// Angle to change
			double winkel = (distanz / (2 * getYM() * Math.PI)) * 360;

			// Distanz der Vorwärtsbewegung
			double forward = c * Math.sin(Math.toRadians((winkel / 2))) * 2;

			// Forward. Winkel / 2 ist hinzugefügt, damit nicht auf der
			// Tangentiallinie bewegt wird, sondern genau der Punkt auf
			// der Kreislinie erreicht wird. (Kreisabschnitt)
			x += Math.cos(Math.toRadians(drehung + winkel / 2)) * forward;

			y += Math.sin(Math.toRadians(drehung + winkel / 2)) * forward;

			drehung = (drehung + winkel) % 360; // Neue Drehung des Autos
												// berechnet

		} else { // Gerade Bewegung
			x += Math.cos(Math.toRadians(drehung)) * (v * sec);

			y += Math.sin(Math.toRadians(drehung)) * (v * sec);
		}
	}

	public String toString() {
		return "alpha: " + alpha + ", beta: " + beta + " v: " + v
				+ ", Zentripetalkraft: " + getZentripetal();
	}

	public void draw(GL2 gl, boolean drawLines) {

		gl.glColor3d(1, 1, 1);

		gl.glPushMatrix();

		gl.glTranslated(x, y, 0);

		gl.glRotated(drehung, 0, 0, 1);

		gl.glBegin(GL.GL_LINES);

		gl.glVertex2d(2.5, 0);// Vorderachse
		gl.glVertex2d(2.5, -1.8);// Vorderachse

		gl.glVertex2d(0, 0);// Hinterachse
		gl.glVertex2d(0, -1.8);// Hinterachse

		gl.glVertex2d(-0.3, 0);// Linkes hinterrad
		gl.glVertex2d(0.3, 0);// Linkes hinterrad

		gl.glVertex2d(-0.3, -1.8);// Rechtes hinterrad
		gl.glVertex2d(0.3, -1.8);// Rechtes hinterrad

		// Rahmen
		gl.glVertex2d(3, -0.2);
		gl.glVertex2d(3, -1.6);

		gl.glVertex2d(3, -1.6);
		gl.glVertex2d(-0.2, -1.6);

		gl.glVertex2d(-0.2, -1.6);
		gl.glVertex2d(-0.2, -0.2);

		gl.glVertex2d(-0.2, -0.2);
		gl.glVertex2d(3, -0.2);

		// Vorderräder
		gl.glEnd();

		gl.glPushMatrix();

		gl.glTranslated(2.5, 0, 0);
		gl.glRotated(alpha, 0, 0, 1);
		gl.glBegin(GL.GL_LINES); // Linkes vorderrad

		gl.glVertex2d(-0.3, 0);
		gl.glVertex2d(0.3, 0);
		gl.glEnd();

		gl.glPopMatrix();

		gl.glPushMatrix();

		gl.glTranslated(2.5, -1.8, 0);
		gl.glRotated(beta, 0, 0, 1);
		gl.glBegin(GL.GL_LINES); // rechtes vorderrad

		gl.glVertex2d(-0.3, 0);
		gl.glVertex2d(0.3, 0);
		gl.glEnd();
		gl.glPopMatrix();

		if (drawLines) {

			gl.glColor3d(1, 0, 0);

			double c = getYM() - halbeAchsenbreite;

			gl.glBegin(GL.GL_LINES);

			gl.glVertex2d(0, -0.9);
			gl.glVertex2d(0, c);

			gl.glVertex2d(2.5, 0);
			gl.glVertex2d(0, c);

			gl.glEnd();
		}

		gl.glPopMatrix();
	}
	
	public Car(double x, double y) {
		this.x = x;
		this.y = y;
		
	}
}