package ch.arnosti.pfcs.auto;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JFrame;
import javax.swing.JLabel;

import ch.arnosti.pfcs.utils.Utilities;

import com.jogamp.opengl.util.FPSAnimator;

import com.jogamp.opengl.util.awt.TextRenderer;

public class Auto implements GLEventListener, KeyListener {

	// OpenGL variables
	private double height; // calculated
	private double width = 100f; // m

	private boolean running = true;

	private int fps = 60;

	TextRenderer renderer;

	private Car car = new Car(-(width / 2) + 4, 14);

	// Physics etc.

	private static final double maxZentripedal = 50;

	public Auto() // Konstruktor
	{
		JFrame f = new JFrame("Auto in Kurve");
		f.setLayout(new BorderLayout());

		GLCanvas canvas = new GLCanvas(); // OpenGL-Window
		canvas.addGLEventListener(this);

		// Add Canvas
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.add(canvas, BorderLayout.CENTER);

		f.setVisible(true);

		f.setSize(800, 600);

		canvas.addKeyListener(this);

		FPSAnimator anim = new FPSAnimator(canvas, fps, true);
		anim.start();
		
		JLabel man = new JLabel();
		man.setText("<html>In dieser Übung kann das Auto folgendermassen gesteuert werden:<br/>- Links / rechts: Lenkung<br/>- Hoch / runter: Beschleunigen bzw. bremsen<br/>- s: Berechnungen unterbrechen bzw. fortsetzen<br/>- r: Zurücksetzen</html>");
		f.add(man, BorderLayout.SOUTH);

	}

	// --------- OpenGL-Events -----------------------

	public void init(GLAutoDrawable drawable) {
		GL gl0 = drawable.getGL(); // OpenGL-Objekt
		GL2 gl = gl0.getGL2();
		gl.glEnable(GL2.GL_POINT_SMOOTH);
		gl.glEnable(GL2.GL_LINE_SMOOTH);

		renderer = new TextRenderer(new Font("Arial", Font.BOLD, 10));

	}

	public void display(GLAutoDrawable drawable) {
		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();

		gl.glMatrixMode(GL2.GL_MODELVIEW);

		gl.glLoadIdentity();

		Utilities.drawBackground(gl, height, width);

		drawCurve(gl);

		if (running)
			car.calculateStep(1.0 / fps);

		car.draw(gl, true);

		if (car.getZentripetal() > maxZentripedal) {
			gl.glColor3d(1.0, 0, 0);
		} else {
			gl.glColor3d(0, 1, 0);
		}

		gl.glBegin(GL2.GL_POLYGON);

		// get pixel width in OpenGL Coordinates
		double pixelwidth = height / drawable.getHeight();

		gl.glVertex2d(-width / 2 + 10 * pixelwidth, -height / 2 + 8
				* pixelwidth);
		gl.glVertex2d(-width / 2 + (10 + car.getZentripetal()) * pixelwidth,
				-height / 2 + 8 * pixelwidth);
		gl.glVertex2d(-width / 2 + (10 + car.getZentripetal()) * pixelwidth,
				-height / 2 + 16 * pixelwidth);
		gl.glVertex2d(-width / 2 + 10 * pixelwidth, -height / 2 + 16
				* pixelwidth);

		gl.glEnd();

		renderer.beginRendering(drawable.getWidth(), drawable.getHeight());
		renderer.draw(car.toString(), 10, 20);
		renderer.endRendering();

	}

	public void reshape(GLAutoDrawable drawable, // Window resized
			int x, int y, int width, int height) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glViewport(0, 0, width, height); // Window
		double aspect = (double) height / width;
		double left = -(this.width / 2), right = (this.width / 2);
		this.height = this.width * aspect;
		double bottom = (-this.height / 2), top = (this.height / 2);
		double near = -100, far = 100;
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrtho(left, right, bottom, top, near, far); // ViewingVolume
	}

	public void dispose(GLAutoDrawable drawable) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			if (running)
				car.goFaster();
			break;
		case KeyEvent.VK_DOWN:
			if (running)
				car.goSlower();
			break;
		case KeyEvent.VK_LEFT:
			if (running)
				car.turnLeft();
			break;
		case KeyEvent.VK_RIGHT:
			if (running)
				car.turnRight();
			break;
		case KeyEvent.VK_R:
			car = new Car(-(width / 2) + 4, 14);
			break;
		case KeyEvent.VK_S:
			running = !running;
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		System.out.println("nope");
	}

	public void drawCurve(GL2 gl) {
		gl.glLoadIdentity();

		gl.glTranslated(-(width / 2), 15.5, 0);

		gl.glColor3d(1, 1, 1);

		double alpha = 0;

		// first strip
		for (int i = 0; i < width / 2; i++) {
			gl.glBegin(GL2.GL_LINE_STRIP);
			gl.glVertex2d(0, 0);
			gl.glVertex2d(1, 0);
			gl.glEnd();
			gl.glTranslated(1, 0, 0);
		}

		gl.glColor3d(1, 0, 1);

		for (int i = 0; i < 20; i++) {
			alpha -= 0.2;
			gl.glRotated(alpha, 0, 0, 1);
			gl.glBegin(GL2.GL_LINE_STRIP);
			gl.glVertex2d(0, 0);
			gl.glVertex2d(1, 0);
			gl.glEnd();
			gl.glTranslated(1, 0, 0);
		}

		gl.glColor3d(1, 0, 0);

		for (int i = 0; i < 25; i++) {
			gl.glRotated(alpha, 0, 0, 1);
			gl.glBegin(GL2.GL_LINE_STRIP);
			gl.glVertex2d(0, 0);
			gl.glVertex2d(1, 0);
			gl.glEnd();
			gl.glTranslated(1, 0, 0);
		}

		gl.glColor3d(1, 1, 0);

		for (int i = 0; i < 20; i++) {
			alpha += 0.2;
			gl.glRotated(alpha, 0, 0, 1);
			gl.glBegin(GL2.GL_LINE_STRIP);
			gl.glVertex2d(0, 0);
			gl.glVertex2d(1, 0);
			gl.glEnd();
			gl.glTranslated(1, 0, 0);
		}

		gl.glColor3d(1, 1, 1);

		
		for (int i = 0; i < width / 2; i++) {
			gl.glBegin(GL2.GL_LINE_STRIP);
			gl.glVertex2d(0, 0);
			gl.glVertex2d(1, 0);
			gl.glEnd();
			gl.glTranslated(1, 0, 0);
		}
		gl.glLoadIdentity();

		gl.glTranslated(-(width / 2), 11, 0);

		for (int i = 0; i < width / 2; i++) {
			gl.glBegin(GL2.GL_LINE_STRIP);
			gl.glVertex2d(0, 0);
			gl.glVertex2d(1, 0);
			gl.glEnd();
			gl.glTranslated(1, 0, 0);
		}

		for (int i = 0; i < 20; i++) {
			alpha -= 0.2;
			gl.glRotated(alpha, 0, 0, 1);
			gl.glBegin(GL2.GL_LINE_STRIP);
			gl.glVertex2d(0, 0);
			gl.glVertex2d(0.7, 0);
			gl.glEnd();
			gl.glTranslated(0.7, 0, 0);
		}

		for (int i = 0; i < 25; i++) {
			gl.glRotated(alpha, 0, 0, 1);
			gl.glBegin(GL2.GL_LINE_STRIP);
			gl.glVertex2d(0, 0);
			gl.glVertex2d(0.7, 0);
			gl.glEnd();
			gl.glTranslated(0.7, 0, 0);
		}

		for (int i = 0; i < 20; i++) {
			alpha += 0.2;
			gl.glRotated(alpha, 0, 0, 1);
			gl.glBegin(GL2.GL_LINE_STRIP);
			gl.glVertex2d(0, 0);
			gl.glVertex2d(0.7, 0);
			gl.glEnd();
			gl.glTranslated(0.7, 0, 0);
		}

		for (int i = 0; i < width / 2; i++) {
			gl.glBegin(GL2.GL_LINE_STRIP);
			gl.glVertex2d(0, 0);
			gl.glVertex2d(1, 0);
			gl.glEnd();
			gl.glTranslated(1, 0, 0);
		}
		gl.glLoadIdentity();

	}

}