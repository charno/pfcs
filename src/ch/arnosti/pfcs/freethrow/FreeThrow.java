package ch.arnosti.pfcs.freethrow;

//  -------------    JOGL SampleProgram  (Pyramide) ------------
//
//     Darstellung einer Pyramide mit Kamera- und Objekt-System
//
import javax.media.opengl.*;

import java.awt.event.*;
import java.util.Iterator;
import java.util.LinkedList;

import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.fixedfunc.GLMatrixFunc;
import javax.swing.JFrame;

import ch.arnosti.pfcs.utils.Utilities;

import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.gl2.GLUT;

public class FreeThrow implements GLEventListener, KeyListener {
	private GLCanvas canvas; // OpenGl-Canvas
	double left = -4, right = 4; // Koordinatenbereich
	double bottom, top; // werden in reshape gesetzt
	double near = -100, far = 100; // Clipping Bereich
	private volatile double elev = 16; // Elevation Kamera-System
	private volatile double azim = 40; // Azimut Kamera-System

	private double v = 10;
	private double alpha = 45;
	private double x = left, y = 1;

	private int fps = 60;

	private java.util.List<Body> list = new LinkedList<Body>();

	private GLUT glut;

	// ------------------ Methoden --------------------

	public void rotateCam(GL2 gl, double phi, double nx, double ny, double nz) {
		gl.glRotated(-phi, nx, ny, nz); // Nur der Winkel muss invers sein,
										// damit das Kamerasystem gedreht wird.
	}

	public void translateCam(GL2 gl, double dx, double dy, double dz) {
		gl.glTranslated(-dx, -dy, -dz); // Aller Achsen drehen - Objektsystem in
										// antgegengesetzter Richtung drehen.s
	}

	public FreeThrow() {
		JFrame f = new JFrame("FreeThrow");
		canvas = new GLCanvas(); // OpenGL-Window
		f.setSize(800, 600);
		canvas.addKeyListener(this);
		canvas.addGLEventListener(this);
		f.add(canvas);
		f.setVisible(true);
	}

	// --------- OpenGL-Events -----------------------

	public void init(GLAutoDrawable drawable) {
		GL gl0 = drawable.getGL(); // OpenGL-Objekt
		GL2 gl = gl0.getGL2();
		gl.glClearColor(0.6f, 0.6f, 0.6f, 1.0f); // erasing color
		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glEnable(GL2.GL_NORMALIZE);
		gl.glEnable(GL2.GL_LIGHT0);

		FPSAnimator anim = new FPSAnimator(drawable, fps, true);
		anim.start();

		glut = new GLUT();
	}

	@Override
	public void display(GLAutoDrawable drawable) {

		float[] lightPos = { -10, 150, 100, 1 }; // absolutes system

		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT); // Clear
																		// Color
																		// Buffer
																		// (oder
																		// setzen
																		// wenn
																		// der
																		// GL_DEPTH_TEST
																		// an
																		// ist)
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		gl.glLoadIdentity();
		// Kamerasystem zuerst machen
		translateCam(gl, 0, 1, 2);
		rotateCam(gl, -elev, 1, 0, 0); // nach oben
		rotateCam(gl, azim, 0, 1, 0);

		gl.glDisable(GL2.GL_LIGHTING); // achsen sollen nicht speziell behandelt
										// werden bzgl licht
		Utilities.zeichneAchsen(gl, 6);

		gl.glEnable(GL2.GL_LIGHTING); // rest soll beleuchtet werden
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, lightPos, 0);
		for (Iterator<Body> it = list.iterator(); it.hasNext();) {
			Body body = it.next();
			if (body.disposable(right * 2, bottom * 2)) {
				it.remove();
				continue;
			}
			body.draw(gl, glut);
			body.move(0.2 / fps);

		}
	}

	private void resetMaterial(GL2 gl) {
		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE,
				new float[] { 0.8f, 0.8f, 0.8f, 1 }, 0);
	}

	public void reshape(GLAutoDrawable drawable, // Window resized
			int x, int y, int width, int height) {
		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();
		gl.glViewport(0, 0, width, height);
		double aspect = (float) height / width; // aspect-ratio
		bottom = aspect * left;
		top = aspect * right;
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrtho(left, right, bottom, top, near, far); // Viewing-Volume (im
															// Raum)
	}

	public void dispose(GLAutoDrawable drawable) {
	}

	// --------- Window-Events --------------------

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		// cam
		case KeyEvent.VK_UP:
			elev += 1;
			break;
		case KeyEvent.VK_DOWN:
			elev -= 1;
			break;
		case KeyEvent.VK_LEFT:
			azim -= 1;
			break;
		case KeyEvent.VK_RIGHT:
			azim += 1;
			break;
		case KeyEvent.VK_3:
			list.add(new Ball(v, alpha, x, y));
			break;
		case KeyEvent.VK_1:
			list.add(new Quader(v, alpha, x, y, 5, 20, 10, 0.3, 0.1, 0.3));
			break;
		case KeyEvent.VK_2:
			list.add(new Torus(v, alpha, x, y, 0, 5, 20));
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

}