package ch.arnosti.pfcs.freethrow;

import javax.media.opengl.GL2;

import ch.arnosti.pfcs.dynamics.Dynamics;

import com.jogamp.opengl.util.gl2.GLUT;

public class Torus extends Body {


	private final static double R = 0.25;
	private final static double r = 0.125;

	public Torus(double v, double alpha, double x, double y, double oa,
			double ob, double oc) {
		super(v, alpha, x, y, oa, ob, oc);

		dynamics = new TorusDynamics();
	}

	@Override
	public void draw(GL2 gl, GLUT glut) {
		gl.glPushMatrix();
		gl.glTranslated(x, y, 0);
		gl.glRotated(Math.toDegrees(state2[0]), state2[1], state2[2], state2[3]);
		glut.glutSolidTorus(r, R, 32, 64);
		gl.glPopMatrix();
	}

	public class TorusDynamics extends Dynamics {
		public final double m = 1000; // g

		public final double i1;
		public final double i2;
		public final double i3;

		public TorusDynamics() {
			i1 = i2 = (1.0 / 2.0) * m * R * R + (5.0/8.0) * m * r * r; 
			i3 = m * R * R + (3.0 / 4.0) * m * r * r;
		}

		@Override
		public double[] f(double[] x) {
			return new double[] { (1.0 / i1) * (i2 - i3) * x[o2] * x[o3],
					(1.0 / i2) * (i3 - i1) * x[o3] * x[o1],
					(1.0 / i3) * (i1 - i2) * x[o1] * x[o2],
					-1d / 2d * (x[q1] * x[o1] + x[q2] * x[o2] + x[q3] * x[o3]),
					1d / 2d * (x[q0] * x[o1] + x[q2] * x[o3] - x[q3] * x[o2]),
					1d / 2d * (x[q0] * x[o2] + x[q3] * x[o1] - x[q1] * x[o3]),
					1d / 2d * (x[q0] * x[o3] + x[q1] * x[o2] - x[q2] * x[o1]) };
		}

	}

}
