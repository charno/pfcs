package ch.arnosti.pfcs.freethrow;

import javax.media.opengl.GL2;

import com.jogamp.opengl.util.gl2.GLUT;

public class Ball extends Body {

	public Ball(double v, double alpha, double x, double y) {
		super(v, alpha, x, y, 0, 0, 0);
	}

	@Override
	public void draw(GL2 gl, GLUT glut) {
		gl.glPushMatrix();
		gl.glTranslated(x, y, 0);
		glut.glutSolidSphere(0.1, 10, 10);
		gl.glPopMatrix();
	}

	@Override
	public void move(double dt) {
		// This is implemented so that the dynamics call is not performed
		movebody(dt);
	}

}
