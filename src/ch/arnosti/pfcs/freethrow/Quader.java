package ch.arnosti.pfcs.freethrow;

import javax.media.opengl.GL2;

import ch.arnosti.pfcs.dynamics.Dynamics;

import com.jogamp.opengl.util.gl2.GLUT;

public class Quader extends Body {

	private double a;
	private double b;
	private double c;


	public Quader(double v, double alpha, double x, double y, double oa,
			double ob, double oc, double a, double b, double c) {
		super(v, alpha, x, y, oa, ob, oc);
		this.a = a;
		this.b = b;
		this.c = c;

		dynamics = new QuaderDynamics();
	}

	@Override
	public void draw(GL2 gl, GLUT glut) {
		gl.glPushMatrix();
		gl.glTranslated(x, y, 0);
		gl.glRotated(Math.toDegrees(state2[0]), state2[1], state2[2], state2[3]);
		gl.glScaled(a, b, c);
		glut.glutSolidCube(1);
		gl.glPopMatrix();
	}

	public class QuaderDynamics extends Dynamics {
		public final double m = 1000; // g

		public final double i1;
		public final double i2;
		public final double i3;

		public QuaderDynamics() {
			i1 = (1.0 / 12.0) * m * (b * b + c * c);
			i2 = (1.0 / 12.0) * m * (a * a + c * c);
			i3 = (1.0 / 12.0) * m * (a * a + b * b);
		}

		@Override
		public double[] f(double[] x) {
			return new double[] { (1.0 / i1) * (i2 - i3) * x[o2] * x[o3],
					(1.0 / i2) * (i3 - i1) * x[o3] * x[o1],
					(1.0 / i3) * (i1 - i2) * x[o1] * x[o2],
					-1d / 2d * (x[q1] * x[o1] + x[q2] * x[o2] + x[q3] * x[o3]),
					1d / 2d * (x[q0] * x[o1] + x[q2] * x[o3] - x[q3] * x[o2]),
					1d / 2d * (x[q0] * x[o2] + x[q3] * x[o1] - x[q1] * x[o3]),
					1d / 2d * (x[q0] * x[o3] + x[q1] * x[o2] - x[q2] * x[o1]) };
		}

	}

}
