package ch.arnosti.pfcs.freethrow;

import javax.media.opengl.GL2;

import ch.arnosti.pfcs.dynamics.Dynamics;

import com.jogamp.graph.math.Quaternion;
import com.jogamp.opengl.util.gl2.GLUT;

public abstract class Body {

	protected double x, y; // Position
	private double vx, vy; // Geschwindigkeit
	private final double g = 9.81; // Erdbeschleunigung m/s^2
	
	protected double[] state = new double[7];
	protected double[] state2 = new double[4];
	
	// Indices
	public static final int o1 = 0;
	public static final int o2 = 1;
	public static final int o3 = 2;
	public static final int q0 = 3;
	public static final int q1 = 4;
	public static final int q2 = 5;
	public static final int q3 = 6;
	
	protected Dynamics dynamics;

	public Body(double v, double alpha, double x, double y, double oa, double ob, double oc) {
		this.x = x;
		this.y = y;
		this.vx = v * Math.cos(2 * Math.PI * alpha / 360);
		this.vy = v * Math.sin(2 * Math.PI * alpha / 360);
		state[o1] = oa;
		state[o2] = ob;
		state[o3] = oc;
		state[q0] = 1;
		state[q1] = 0;
		state[q2] = 0;
		state[q3] = 0;
	}
	
	protected void movebody(double dt) {
		y += vy * dt;
		vy += -g * dt;
		x += vx * dt;
	}

	public boolean disposable(double bx, double by) {
		return (y < by || x > bx);
	}

	public abstract void draw(GL2 gl, GLUT glut);

	public void move(double dt) {
		movebody(dt);
		state = dynamics.runge(state, dt);

		// Normalisiere das Quaternion (Sicherheit wegen Double-Ungenaugkeit)
		double ql = Math.sqrt(state[q0] * state[q0] + state[q1] * state[q1]
				+ state[q2] * state[q2] + state[q3] * state[q3]);
		state[q0] = state[q0] / ql;
		state[q1] = state[q1] / ql;
		state[q2] = state[q2] / ql;
		state[q3] = state[q3] / ql;

		// Befülle state2
		ql = Math.sqrt(state[q1] * state[q1] + state[q2] * state[q2]
				+ state[q3] * state[q3]);

		state2[0] = Math.acos(state[q0]) * 2;
		state2[1] = state[q1] / ql;
		state2[2] = state[q2] / ql;
		state2[3] = state[q3] / ql;
	}
}
