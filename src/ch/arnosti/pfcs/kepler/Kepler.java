package ch.arnosti.pfcs.kepler;

//
//     Darstellung einer Kugel mit Kamera- und Objekt-System
//
import javax.media.opengl.*;

import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;

import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.fixedfunc.GLMatrixFunc;
import javax.media.opengl.glu.GLU;

import ch.arnosti.pfcs.dynamics.Dynamics;

import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.awt.TextRenderer;
import com.jogamp.opengl.util.gl2.GLUT;

public class Kepler implements WindowListener, GLEventListener, KeyListener {
	GLCanvas canvas; // OpenGl-Canvas
	double left = -100, right = 100; // Koordinatenbereich
	double bottom, top; // werden in reshape gesetzt
	double near = -200, far = 200; // Clipping Bereich
	private volatile double elev = 14; // Elevation Kamera-System
	private volatile double azim = 40; // Azimut Kamera-System
	private volatile double dist = 100; // Abstand Kamera von O (ohne Bedeutung
	// bei Orthogonalprjektion)
	
	TextRenderer renderer;


	// Physik: Längeneinheit E = 1.0e6 m
	double g = 9.81e-6; // E/s^2
	double radiusEarth = 6;
	double GM = g * radiusEarth * radiusEarth; // G*M
	double orbit = 35.68 + radiusEarth; // Höhe ab Mittelpunkt, Geostationär
	double vOrig = Math.sqrt(GM / orbit);

	private GLUT glut;
	private int height, width;

	private KeplerDynamics kepler = new KeplerDynamics();
	double[] x = { orbit, 0, 0, vOrig }; // x1, x2, v1, v2
	double dt = 100;

	java.util.List<double[]> points = new LinkedList<double[]>();

	public void zeichneErde(GL2 gl, double radius) {
		glut.glutSolidSphere(radius, 50, 50);
	}

	// ------------------ Methoden --------------------

	public void rotateCam(GL2 gl, double phi, double nx, double ny, double nz) {
		gl.glRotated(-phi, nx, ny, nz); // Nur der Winkel muss invers sein,
										// damit das Kamerasystem gedreht wird.
	}

	public void translateCam(GL2 gl, double dx, double dy, double dz) {
		gl.glTranslated(-dx, -dy, -dz); // Aller Achsen drehen - Objektsystem in
										// entgegengesetzter Richtung drehen.
	}

	void zeichneAchsen(GL2 gl, double a) // Koordinatenachsen zeichnen
	{
		gl.glBegin(GL.GL_LINES);
		gl.glVertex3d(0, 0, 0);
		gl.glVertex3d(a, 0, 0);
		gl.glVertex3d(0, 0, 0);
		gl.glVertex3d(0, a, 0);
		gl.glVertex3d(0, 0, 0);
		gl.glVertex3d(0, 0, a);
		gl.glEnd();
	}
	
	void zeichneOrbit(GL2 gl, double v) // Koordinatenachsen zeichnen
	{
		gl.glBegin(GL.GL_LINES);
		gl.glEnd();
	}

	public Kepler() {
		Frame f = new Frame("MySecond");
		canvas = new GLCanvas(); // OpenGL-Window
		f.setSize(800, 600);
		f.setBackground(Color.gray);
		f.addWindowListener(this);
		f.addKeyListener(this);
		canvas.addGLEventListener(this);
		canvas.addKeyListener(this);
		f.add(canvas);
		f.setVisible(true);
	}

	public static void main(String[] args) {
		new Kepler();
	}

	// --------- OpenGL-Events -----------------------

	public void init(GLAutoDrawable drawable) {
		GL gl0 = drawable.getGL(); // OpenGL-Objekt
		GL2 gl = gl0.getGL2();
		gl.glClearColor(0.6f, 0.6f, 0.6f, 1.0f); // erasing color
		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glEnable(GL2.GL_NORMALIZE);
		gl.glEnable(GL2.GL_LIGHT0);

		gl.glShadeModel(GL2.GL_FLAT);

		FPSAnimator anim = new FPSAnimator(drawable, 60, true);
		anim.start();

		glut = new GLUT();
		renderer = new TextRenderer(new Font("Arial", Font.BOLD, 10));

	}

	@Override
	public void display(GLAutoDrawable drawable) {
		// konvention: licht sollte von oben links kommen
		float[] lightPos = { -10, 150, 100, 1 }; // absolutes system

		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT); // Clear
																		// Color
																		// Buffer
																		// (oder
																		// setzen
																		// wenn
																		// der
																		// GL_DEPTH_TEST
																		// an
																		// ist)
		gl.glColor3d(0, 1, 1); // Zeichenfarbe
		// wenn der PolygonMode nicht gesetzt wird dann werden die Polygone
		// ausgefüllt gezeichnet
		// gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_LINE); // Front and Back
		// hat beschreibt die Beleuchtung. Polygon Zeichen-Modus
		gl.glColor3d(1, 1, 1); // Zeichenfarbe
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		gl.glLoadIdentity();

		// Kamerasystem zuerst machen
		// Azimuth
		translateCam(gl, 0, 0, -dist);
		rotateCam(gl, -elev, 1, 0, 0); // nach oben
		rotateCam(gl, azim, 0, 1, 0);

		// System.out.println("elev:" + elev + ", azim:" + azim);

		// Elevation
		// 1. translate in z-Richtung
		// 2. Drehung um die X-Achse, -elevation
		// 3. Drehung um die Y-Achse, azimut

		gl.glDisable(GL2.GL_LIGHTING); // achsen sollen nicht speziell behandelt
										// werden bzgl licht
		zeichneAchsen(gl, 60);

		gl.glEnable(GL2.GL_LIGHTING); // pyramide soll beleuchtet werden
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, lightPos, 0);

		gl.glColor3d(1, 1, 1);

		zeichneErde(gl, radiusEarth);

		gl.glTranslated(x[0], x[1], 0);

		gl.glColor3d(1, 0, 0);
		glut.glutSolidSphere(0.5, 20, 30);

		x = kepler.runge(x, dt);
		
		renderer.beginRendering(drawable.getWidth(), drawable.getHeight());
		renderer.draw("x1=" + x[0], 10, 40);
		renderer.draw("x2="+x[1], 10, 30);
		renderer.draw("v1=" + x[2] , 10, 20);
		renderer.draw("v2="+x[3] , 10, 10);

		renderer.draw("vTot=" + Math.sqrt(x[3]*x[3]+x[2]*x[2]) , 10, 70);
		renderer.draw("rTot=" + Math.sqrt(x[0]*x[0]+x[1]*x[1]) , 10, 60);

		
		renderer.endRendering();

	}

	public void reshape(GLAutoDrawable drawable, // Window resized
			int x, int y, int width, int height) {
		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();
		gl.glViewport(0, 0, width, height);
		double aspect = (float) height / width; // aspect-ratio
		bottom = aspect * left;
		top = aspect * right;
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrtho(left, right, bottom, top, near, far); // Viewing-Volume (im
															// Raum)

		this.height = height;
		this.width = width;
	}

	public void dispose(GLAutoDrawable drawable) {
	}

	// --------- Window-Events --------------------

	public void windowClosing(WindowEvent e) {
		System.exit(0);
	}

	public void windowActivated(WindowEvent e) {
	}

	public void windowClosed(WindowEvent e) {
	}

	public void windowDeactivated(WindowEvent e) {
	}

	public void windowDeiconified(WindowEvent e) {
	}

	public void windowIconified(WindowEvent e) {
	}

	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			x[2] += 0.0001;
			break;
		case KeyEvent.VK_DOWN:
			x[2] -= 0.0001;
			break;
		case KeyEvent.VK_LEFT:
			x[3] += 0.0001;
			break;
		case KeyEvent.VK_RIGHT:
			x[3] -= 0.0001;
			break;
		case KeyEvent.VK_R:
			x = new double[] { orbit, 0, 0, vOrig };
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	class KeplerDynamics extends Dynamics {
		@Override
		public double[] f(double[] x) {

			double r = Math.sqrt(x[0] * x[0] + x[1] * x[1]);
			double r3 = r * r * r;

			double[] ret = { x[2], // Delta x
					x[3], // Delta y
					-GM / r3 * x[0], -GM / r3 * x[1] };

			return ret;
		}

	}

}