package ch.arnosti.pfcs.gyro;

//  -------------    JOGL SampleProgram  (Pyramide) ------------
//
//     Darstellung einer Pyramide mit Kamera- und Objekt-System
//
import javax.media.opengl.*;

import java.awt.*;
import java.awt.event.*;

import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.fixedfunc.GLMatrixFunc;

import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.gl2.GLUT;

public class Gyroskop implements WindowListener, GLEventListener, KeyListener {
	private GLCanvas canvas; // OpenGl-Canvas
	double left = -4, right = 4; // Koordinatenbereich
	double bottom, top; // werden in reshape gesetzt
	double near = -100, far = 100; // Clipping Bereich
	private volatile double elev = 16; // Elevation Kamera-System
	private volatile double azim = 40; // Azimut Kamera-System
	
	private volatile int gyro_elev = 0;
	private volatile int gyro_azim = 0;
	private volatile int rot_speed = 176; // [°/s]
	private volatile float gyro_rot = 0;
	
	private volatile long lastFrameTime;

	private GLUT glut;

	// ------------------ Methoden --------------------
	
    public void rotateCam(GL2 gl, double phi, double nx, double ny, double nz) {
        gl.glRotated(-phi, nx, ny, nz); // Nur der Winkel muss invers sein, damit das Kamerasystem gedreht wird.
    }

    public void translateCam(GL2 gl, double dx, double dy, double dz) {
        gl.glTranslated(-dx, -dy, -dz); // Aller Achsen drehen - Objektsystem in antgegengesetzter Richtung drehen.s
    }
	
	void zeichneAchsen(GL2 gl, double a) {
		gl.glBegin(GL.GL_LINES);
		gl.glVertex3d(0, 0, 0);
		gl.glVertex3d(a, 0, 0);
		gl.glVertex3d(0, 0, 0);
		gl.glVertex3d(0, a, 0);
		gl.glVertex3d(0, 0, 0);
		gl.glVertex3d(0, 0, a);
		gl.glEnd();
	}

	private void zeichneGyro(GL2 gl) {
		// "füsse"
		gl.glPushMatrix();
		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE, new float[]{0,1,0,1}, 0);
		for (int i=0; i<3; i++) {
			gl.glRotatef(120, 0, 1, 0);
			gl.glTranslatef(1, 0, 0);
			glut.glutSolidSphere(0.1, 30, 30);
			gl.glTranslatef(-1, 0, 0);
		}
		gl.glPopMatrix();
		
		// beine/gestell
		gl.glPushMatrix();
		
		// gelenk oben
		gl.glTranslatef(0, 2f, 0);
		gl.glRotatef(90, 1, 0, 0);

		glut.glutSolidSphere(0.075, 30, 30);
		resetMaterial(gl);
		glut.glutSolidCylinder(0.05, 1.5, 30, 30);
		gl.glRotatef(-90, 1, 0, 0);
		
		// punkt an dem beine zusammenkommen
		gl.glTranslatef(0, -1.5f, 0);
		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE, new float[]{0,1,0,1}, 0);
		glut.glutSolidSphere(0.1, 30, 30);
		resetMaterial(gl);
		gl.glRotatef(90, 0, 1, 0);
		// beine
		for (int i=0; i<3; i++) {
			gl.glRotatef(26, 1, 0, 0);
			glut.glutSolidCylinder(0.05, 1.2, 30, 30);
			gl.glRotatef(-26, 1, 0, 0);
			gl.glRotatef(120, 0, 1, 0);
		}
		gl.glPopMatrix();
		
		// obere stange
		gl.glPushMatrix();
		gl.glTranslatef(0, 2f, 0);
		gl.glRotatef(gyro_azim, 0, 1, 0);
		gl.glRotatef(gyro_elev, 1, 0, 0);
		
		// hierhin kommt die rotierende scheibe
		gl.glTranslatef(0, 0, -1.25f);
		rotiereScheibe(gl);
		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE, new float[]{0,0,1,1}, 0);
		glut.glutSolidCylinder(0.75, 0.05, 30, 30); // scheibe
		
		// markierungen auf scheibe
		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE, new float[]{1,0,0,1}, 0);
		gl.glTranslatef(0.5f, 0, 0);
		glut.glutSolidCylinder(0.05, 0.075, 30, 30);
		gl.glTranslatef(-1f, 0, 0);
		glut.glutSolidCylinder(0.05, 0.075, 30, 30);
		gl.glTranslatef(0.5f, 0, 0);
		resetMaterial(gl);
		
		// rotation aufheben... kanten von sich drehenden
		// zylindern flimmern
		gl.glRotatef(-gyro_rot, 0, 0, 1);		
		glut.glutSolidCylinder(0.025, 3, 30, 30); // stange
		
		// gegengewichte
		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE, new float[]{0.2f,0.2f,0.2f,1}, 0);
		
		gl.glTranslatef(0, 0, 2.125f);
		glut.glutSolidCylinder(0.25, 0.375, 30, 30);
		gl.glTranslatef(0, 0, 0.5f);
		glut.glutSolidCylinder(0.125, 0.25, 30, 30);
		
		gl.glPopMatrix();
	}
	
	private void rotiereScheibe(GL2 gl) {
		final long elapsedms = System.currentTimeMillis()-lastFrameTime;
		double elapsed = elapsedms/1000d;
		gyro_rot += rot_speed * elapsed;
		lastFrameTime += elapsedms;
		gl.glRotated(gyro_rot, 0, 0, 1);
	}

	public Gyroskop() {
		Frame f = new Frame("Gyroskop, Pascal Schwarz");
		canvas = new GLCanvas(); // OpenGL-Window
		f.setSize(800, 600);
		f.setBackground(Color.gray);
		f.addWindowListener(this);
		f.addKeyListener(this);
		canvas.addGLEventListener(this);
		f.add(canvas);
		f.setVisible(true);
	}

	public static void main(String[] args) {
		new Gyroskop();
	}

	// --------- OpenGL-Events -----------------------

	public void init(GLAutoDrawable drawable) {
		GL gl0 = drawable.getGL(); // OpenGL-Objekt
		GL2 gl = gl0.getGL2();
		gl.glClearColor(0.6f, 0.6f, 0.6f, 1.0f); // erasing color
		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glEnable(GL2.GL_NORMALIZE);
		gl.glEnable(GL2.GL_LIGHT0);
		
		FPSAnimator anim = new FPSAnimator(drawable, 60, true);
		anim.start();
		
		lastFrameTime = System.currentTimeMillis();
		
		glut = new GLUT();
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		
		float[] lightPos = {-10, 150, 100, 1}; //absolutes system
		
        GL gl0 = drawable.getGL();
        GL2 gl = gl0.getGL2();
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT); // Clear Color Buffer (oder setzen wenn der GL_DEPTH_TEST an ist)
        gl.glColor3d(0, 1, 1);                                    // Zeichenfarbe
        // wenn der PolygonMode nicht gesetzt wird dann werden die Polygone ausgefüllt gezeichnet
        //gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_LINE);     // Front and Back hat beschreibt die Beleuchtung. Polygon Zeichen-Modus
        gl.glColor3d(1, 1, 1);                                    // Zeichenfarbe
        gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
        gl.glLoadIdentity();
        
		// material zurücksetzen
		resetMaterial(gl);

        // Kamerasystem zuerst machen
        translateCam(gl, 0, 1, 2);
        rotateCam(gl, -elev, 1, 0, 0); // nach oben
        rotateCam(gl, azim, 0, 1, 0);
        
        // Elevation
        // 1. translate in z-Richtung
        // 2. Drehung um die X-Achse, -elevation
        // 3. Drehung um die Y-Achse, azimut

        gl.glDisable(GL2.GL_LIGHTING); // achsen sollen nicht speziell behandelt werden bzgl licht
        zeichneAchsen(gl, 6);

        gl.glEnable(GL2.GL_LIGHTING); // rest soll beleuchtet werden
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, lightPos, 0);
        
        // Dann das Objektsystem drehen individuell
        // Positioniere Objektsystem
        
        zeichneGyro(gl);
	}

	private void resetMaterial(GL2 gl) {
		gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE, new float[]{0.8f,0.8f,0.8f,1}, 0);
	}

	public void reshape(GLAutoDrawable drawable, // Window resized
			int x, int y, int width, int height) {
		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();
		gl.glViewport(0, 0, width, height);
		double aspect = (float) height / width; // aspect-ratio
		bottom = aspect * left;
		top = aspect * right;
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrtho(left, right, bottom, top, near, far); // Viewing-Volume (im Raum)
	}

	public void dispose(GLAutoDrawable drawable) {
	}

	// --------- Window-Events --------------------

	public void windowClosing(WindowEvent e) {
		System.exit(0);
	}

	public void windowActivated(WindowEvent e) {
	}

	public void windowClosed(WindowEvent e) {
	}

	public void windowDeactivated(WindowEvent e) {
	}

	public void windowDeiconified(WindowEvent e) {
	}

	public void windowIconified(WindowEvent e) {
	}

	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		// gyro
		case KeyEvent.VK_LEFT:
			gyro_azim++;
			break;
		case KeyEvent.VK_RIGHT:
			gyro_azim--;
			break;
		case KeyEvent.VK_UP:
			gyro_elev++;
			break;
		case KeyEvent.VK_DOWN:
			gyro_elev--;
			break;
		
		// cam
		case KeyEvent.VK_KP_UP:
			elev+=1;
			break;
		case KeyEvent.VK_KP_DOWN:
			elev-=1;
			break;
		case KeyEvent.VK_KP_LEFT:
			azim-=1;
			break;
		case KeyEvent.VK_KP_RIGHT:
			azim+=1;
			break;

		case KeyEvent.VK_W:
			if (e.isShiftDown()) {
				rot_speed++;
			} else {
				rot_speed--;
			}
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {}

}