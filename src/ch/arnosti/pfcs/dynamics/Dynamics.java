package ch.arnosti.pfcs.dynamics;

public abstract class Dynamics {

	public double[] euler(double[] x, double dt) {

		double[] y = f(x);
		double[] xx = new double[x.length];

		for (int i = 0; i < x.length; i++) {
			xx[i] = x[i] + y[i] * dt;
		}
		return xx;
	}

	public double[] runge(double[] x1, double dt) {

		double[] y1 = f(x1);

		// Erster Hilfsvektor
		double[] x2 = new double[x1.length];
		for (int i = 0; i < x2.length; i++) {
			x2[i] = x1[i] + y1[i] * dt / 2;
		}

		double[] y2 = f(x2);

		// Zweiter Hilfsvektor
		double[] x3 = new double[x2.length];
		for (int i = 0; i < x3.length; i++) {
			x3[i] = x1[i] + y2[i] * dt / 2;

		}

		double[] y3 = f(x3);

		// Dritter Hilfsvektor
		double[] x4 = new double[x2.length];
		for (int i = 0; i < x4.length; i++) {
			x4[i] = x1[i] + y3[i] * dt;

		}

		double[] y4 = f(x4);

		double[] y = new double[x1.length];

		for (int i = 0; i < y.length; i++) {
			y[i] = x1[i] + dt * ((y1[i] + 2 * y2[i] + 2 * y3[i] + y4[i]) / 6);
		}

		return y;

	}

	abstract public double[] f(double[] x);

}
