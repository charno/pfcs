package ch.arnosti.pfcs.dynamics;

//
//     Darstellung einer Kugel mit Kamera- und Objekt-System
//
import javax.media.opengl.*;

import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;

import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.fixedfunc.GLMatrixFunc;
import javax.media.opengl.glu.GLU;

import ch.arnosti.pfcs.utils.Utilities;

import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.gl2.GLUT;

public class Lorenz implements WindowListener, GLEventListener, KeyListener {
	GLCanvas canvas; // OpenGl-Canvas
	double left = -100, right = 100; // Koordinatenbereich
	double bottom, top; // werden in reshape gesetzt
	double near = -100, far = 100; // Clipping Bereich
	private volatile double elev = 14; // Elevation Kamera-System
	private volatile double azim = 40; // Azimut Kamera-System
	private volatile double dist = 4; // Abstand Kamera von O (ohne Bedeutung
	// bei Orthogonalprjektion)

	private GLUT glut;
	private int height, width;
	private int phi = 60;
	private float phi2 = 0;
	private LorenzDynamics lorenz = new LorenzDynamics();
	double[] x = { -1, 2, 1};
	double dt = 0.01;
	
	java.util.List<double[]> points = new LinkedList<double[]>();
	
	
	public void zeichneKurve(GL2 gl) {
		gl.glBegin(GL2.GL_LINE_STRIP);
		for(double[] point : points) {
			gl.glVertex3dv(point, 0);
		}
		gl.glEnd();
		while(points.size() > 1000) {
			points.remove(0);
		}
	}

	// ------------------ Methoden --------------------

	public void rotateCam(GL2 gl, double phi, double nx, double ny, double nz) {
		gl.glRotated(-phi, nx, ny, nz); // Nur der Winkel muss invers sein,
										// damit das Kamerasystem gedreht wird.
	}

	public void translateCam(GL2 gl, double dx, double dy, double dz) {
		gl.glTranslated(-dx, -dy, -dz); // Aller Achsen drehen - Objektsystem in
										// entgegengesetzter Richtung drehen.
	}



	public Lorenz() {
		Frame f = new Frame("MySecond");
		canvas = new GLCanvas(); // OpenGL-Window
		f.setSize(800, 600);
		f.setBackground(Color.gray);
		f.addWindowListener(this);
		f.addKeyListener(this);
		canvas.addGLEventListener(this);
		f.add(canvas);
		f.setVisible(true);
	}

	public static void main(String[] args) {
		new Lorenz();
	}

	// --------- OpenGL-Events -----------------------

	public void init(GLAutoDrawable drawable) {
		GL gl0 = drawable.getGL(); // OpenGL-Objekt
		GL2 gl = gl0.getGL2();
		gl.glClearColor(0.6f, 0.6f, 0.6f, 1.0f); // erasing color
		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glEnable(GL2.GL_NORMALIZE);
		gl.glEnable(GL2.GL_LIGHT0);

		gl.glShadeModel(GL2.GL_FLAT);

		FPSAnimator anim = new FPSAnimator(drawable, 60, true);
		anim.start();

		glut = new GLUT();
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		// konvention: licht sollte von oben links kommen
		float[] lightPos = { -10, 150, 100, 1 }; // absolutes system

		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT); // Clear
																		// Color
																		// Buffer
																		// (oder
																		// setzen
																		// wenn
																		// der
																		// GL_DEPTH_TEST
																		// an
																		// ist)
		gl.glColor3d(0, 1, 1); // Zeichenfarbe
		// wenn der PolygonMode nicht gesetzt wird dann werden die Polygone
		// ausgefüllt gezeichnet
		// gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_LINE); // Front and Back
		// hat beschreibt die Beleuchtung. Polygon Zeichen-Modus
		gl.glColor3d(1, 1, 1); // Zeichenfarbe
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		gl.glLoadIdentity();

		// Kamerasystem zuerst machen
		// Azimuth
		translateCam(gl, 0, 0, 2);
		rotateCam(gl, -elev, 1, 0, 0); // nach oben
		rotateCam(gl, azim, 0, 1, 0);

		// System.out.println("elev:" + elev + ", azim:" + azim);

		// Elevation
		// 1. translate in z-Richtung
		// 2. Drehung um die X-Achse, -elevation
		// 3. Drehung um die Y-Achse, azimut

		gl.glDisable(GL2.GL_LIGHTING); // achsen sollen nicht speziell behandelt
										// werden bzgl licht
		Utilities.zeichneAchsen(gl, 60);

		gl.glEnable(GL2.GL_LIGHTING); // pyramide soll beleuchtet werden
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, lightPos, 0);

		zeichneKurve(gl);
		
		gl.glTranslated(x[0], x[1], x[2]);
		
	gl.glColor3d(1, 0, 0);
		glut.glutSolidSphere(0.5, 20, 30);

		points.add(x = lorenz.runge(x, dt));
		
	}

	public void reshape(GLAutoDrawable drawable, // Window resized
			int x, int y, int width, int height) {
		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();
		gl.glViewport(0, 0, width, height);
		double aspect = (float) height / width; // aspect-ratio
		bottom = aspect * left;
		top = aspect * right;
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrtho(left, right, bottom, top, near, far); // Viewing-Volume (im
															// Raum)

		this.height = height;
		this.width = width;
	}

	public void dispose(GLAutoDrawable drawable) {
	}

	// --------- Window-Events --------------------

	public void windowClosing(WindowEvent e) {
		System.exit(0);
	}

	public void windowActivated(WindowEvent e) {
	}

	public void windowClosed(WindowEvent e) {
	}

	public void windowDeactivated(WindowEvent e) {
	}

	public void windowDeiconified(WindowEvent e) {
	}

	public void windowIconified(WindowEvent e) {
	}

	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			elev += 1;
			break;
		case KeyEvent.VK_DOWN:
			elev -= 1;
			break;
		case KeyEvent.VK_LEFT:
			azim -= 1;
			break;
		case KeyEvent.VK_RIGHT:
			azim += 1;
			break;
		}
		System.out.println("keypressed, elev:" + elev + ", azim:" + azim);
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	static class LorenzDynamics extends Dynamics {
		@Override
		public double[] f(double[] x) {
			double[] ret = { 
					10 * x[1] - 10 * x[0],
					28 * x[0] - x[1] - x[0] * x[2], 
					x[0] * x[1] - x[2] * (8.0 / 3.0) };

			return ret;
		}

	}

}