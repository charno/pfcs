package ch.arnosti.pfcs.flow;

import ch.arnosti.pfcs.dynamics.Dynamics;


public class FlowDynamics extends Dynamics {
	private volatile double R;
	
	public FlowDynamics(double R) {
		setR(R);
	}
	
	public void setR(double R) {
		this.R = R;
	}
	
	@Override
	public double[] f(double[] x) {
		final double x2 = x[0]*x[0];
		final double y2 = x[1]*x[1];
		final double x2_y2 = x2+y2;
		final double xy = x[0]*x[1];
		final double R2 = R*R;
		final double x2_y2x2_y2 = x2_y2*x2_y2;
		
		return new double[] {
			1 + (R2/x2_y2) - ((2*R2*x2)/x2_y2x2_y2),
			-2*R2*xy / x2_y2x2_y2
		};
	}

}
