package ch.arnosti.pfcs.flow;

import java.util.List;

public class ParticleGenerator implements Runnable {
	private final List<FlowParticle> particles;
	private volatile double interval;
	private final double startx, starty, dt;
	private final FlowDynamics dynamics;
	private volatile boolean run = true;
	
	public ParticleGenerator(List<FlowParticle> particles, double interval, double startx, double starty, double dt, FlowDynamics dyn) {
		if (particles == null) throw new IllegalArgumentException("particles must not be null");
		this.particles = particles;
		this.interval = interval;
		this.startx = startx;
		this.starty = starty;
		this.dt = dt;
		this.dynamics = dyn;
	}
	
	public void stop() {
		run = false;
	}
	
	public void setInterval(double interval) {
		this.interval = interval;
	}
	
	@Override
	public void run() {
		while (run) {
			particles.add(new FlowParticle(startx, starty, dynamics, dt));
			try {
				Thread.sleep((long) (interval * 1000));
			} catch (InterruptedException ignore) {	}
		}
	}
}