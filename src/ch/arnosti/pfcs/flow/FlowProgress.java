package ch.arnosti.pfcs.flow;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;

public class FlowProgress extends Thread {
	private final List<FlowParticle> particles;
	private final Semaphore rendering, calculating;
	
	public FlowProgress(List<FlowParticle> particles, Semaphore rendering, Semaphore calculating) {
		this.particles = particles;
		this.rendering = rendering;
		this.calculating = calculating;
	}
	
	@Override
	public void run() {
		while (true) {
			calculating.acquireUninterruptibly();
			synchronized (particles) {
				removeOld();
				
				int q1 = particles.size()/4;
				int middle = particles.size()/2;
				int q3 = q1+middle;
				
				Iterator<FlowParticle> i1 = particles.listIterator();
				Iterator<FlowParticle> i2 = particles.listIterator(q1);
				Iterator<FlowParticle> i3 = particles.listIterator(middle);
				Iterator<FlowParticle> i4 = particles.listIterator(q3);
				
				Thread t1 = new Rechner(i1, 0, q1);
				Thread t2 = new Rechner(i2, q1, middle);
				Thread t3 = new Rechner(i3, middle, q3);
				Thread t4 = new Rechner(i4, q3, particles.size());
				
				t1.start(); t2.start(); t3.start(); t4.start();
				try {
					t1.join(); t2.join(); t3.join(); t4.join();
				} catch (InterruptedException ignore) {}
				
				rendering.release();
			}
		}
	}
	
	private void removeOld() {
		FlowParticle p;
		for (Iterator<FlowParticle> pI = particles.iterator(); pI.hasNext();) {
			p = pI.next();
			if (p.getStepsRendered() > 1000) {
				pI.remove();
			}
		}
	}
	
	private class Rechner extends Thread {
		private final Iterator<FlowParticle> pI;
		private final int start;
		private final int end;
		
		public Rechner(Iterator<FlowParticle> pI, int start, int end) {
			this.pI = pI;
			this.start = start;
			this.end = end;
		}
		
		@Override
		public void run() {
			int i = start;
			while (i < end) {
				FlowParticle p = pI.next();
				p.step();
				i++;
			}
		}
	}
}
