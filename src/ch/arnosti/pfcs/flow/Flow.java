package ch.arnosti.pfcs.flow;

//
//     pfcs Übung 5, Pascal Schwarz
//
import javax.media.opengl.*;

import java.awt.*;
import java.awt.event.*;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.fixedfunc.GLMatrixFunc;

import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.awt.TextRenderer;
import com.jogamp.opengl.util.gl2.GLUT;

public class Flow implements WindowListener, GLEventListener, KeyListener {
	private GLCanvas canvas; // OpenGl-Canvas
	double left = -10, right = 10; // Koordinatenbereich
	double bottom, top; // werden in reshape gesetzt
	double near = -200, far = 200; // Clipping Bereich
	private volatile double elev = 3; // Elevation Kamera-System
	private volatile double azim = 10; // Azimut Kamera-System
	private volatile double dist = 100; // Abstand Kamera von O

	private GLUT glut;
	private TextRenderer tr;

	private final List<FlowParticle> particles = Collections.synchronizedList(new LinkedList<FlowParticle>());
	private final LinkedList<ParticleGenerator> generators = new LinkedList<>();
	private final double startX = -20;
	private double dt = 0.05;
	private volatile double R = 3;
	private final FlowDynamics dyn = new FlowDynamics(R);
	private volatile double particleInterval = 0.15;
	
	private int numSegments = 3;
	private boolean flowprepared;
	
	private final Semaphore calculating = new Semaphore(0);
	private final Semaphore rendering = new Semaphore(0);

	// ------------------ Methoden --------------------
	
    public void rotateCam(GL2 gl, double phi, double nx, double ny, double nz) {
        gl.glRotated(-phi, nx, ny, nz); // Nur der Winkel muss invers sein, damit das Kamerasystem gedreht wird.
    }

    public void translateCam(GL2 gl, double dx, double dy, double dz) {
        gl.glTranslated(-dx, -dy, -dz); // Aller Achsen drehen - Objektsystem in entgegengesetzter Richtung drehen.
    }

	private void zeicheKugel(GL2 gl) {
		gl.glPushMatrix();
		gl.glRotatef(-90, 1f, 0, 0);
		// glut für vorgefertigte Kugel
		int kugelDetail = (int)(10+R*30);
        glut.glutSolidSphere(R, kugelDetail, kugelDetail);
        gl.glPopMatrix();
	}
	
	public Flow() {
		Frame f = new Frame("Übung Strömung, Pascal Schwarz");
		canvas = new GLCanvas(); // OpenGL-Window
		f.setSize(800, 600);
		f.setBackground(Color.gray);
		f.addWindowListener(this);
		canvas.addKeyListener(this);
		canvas.addGLEventListener(this);
		f.add(canvas);
		f.setVisible(true);
	}


	// --------- OpenGL-Events -----------------------

	public void init(GLAutoDrawable drawable) {
		GL gl0 = drawable.getGL(); // OpenGL-Objekt
		GL2 gl = gl0.getGL2();
		tr = new TextRenderer(new Font("Monospace", Font.PLAIN, 12));
		gl.glClearColor(0.15f, 0.15f, 0.15f, 1.0f); // erasing color
		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glEnable(GL2.GL_NORMALIZE);
		gl.glEnable(GL2.GL_LIGHT0);

        gl.glShadeModel(GL2.GL_FLAT);
		
		FPSAnimator anim = new FPSAnimator(drawable, 60, true);
		anim.start();
		
		glut = new GLUT();
		
		for (int i=0; i<10; i++) {
			ParticleGenerator gen = new ParticleGenerator(particles, particleInterval, startX, 0.1 + i*0.3 % 7.35, dt, dyn);
			generators.add(gen);
			new Thread(gen).start();
		}
		
		new FlowProgress(particles, rendering, calculating).start();
		calculating.release();
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		// konvention: licht sollte von oben links kommen
		float[] lightPos = {-10, 150, 100, 1}; //absolutes system
		
        GL2 gl = drawable.getGL().getGL2();
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT); // Clear Color Buffer (oder setzen wenn der GL_DEPTH_TEST an ist)
        gl.glColor3d(1, 1, 1);                                    // Zeichenfarbe
        gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
        gl.glLoadIdentity();
        
        // Kamerasystem zuerst machen
        translateCam(gl, 0, 0, -dist);
        rotateCam(gl, -elev, 1, 0, 0); // nach oben
        rotateCam(gl, azim, 0, 1, 0);
        
        rendering.acquireUninterruptibly();
        flowprepared = false;
        
        gl.glPushMatrix();
        gl.glDisable(GL2.GL_LIGHTING);
        
        zeichneFluss(gl);
        calculating.release();
        
        for (int i=0; i<numSegments; i++) {
            gl.glRotatef(360/numSegments, 0, 0, 1);
        	zeichneFluss(gl);
        }
        gl.glPopMatrix();

        gl.glEnable(GL2.GL_LIGHTING); // weitere objekte sollen beleuchtet werden
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, lightPos, 0);
        
        zeicheKugel(gl);
        
        drawText(drawable);
	}

	private void zeichneFluss(GL2 gl) {
		if (!flowprepared) {
			gl.glNewList(1, GL2.GL_COMPILE);
			synchronized (particles) {
				FlowParticle p;
				for (Iterator<FlowParticle> pI = particles.iterator(); pI.hasNext();) {
					p = pI.next();
					p.zeichneSpur(gl);
				}
			}
			gl.glEndList();
			flowprepared = true;
		}
		gl.glCallList(1);
	}
	
	private void resetParticles() {
		synchronized (particles) {
			for (FlowParticle fp : particles) {
				fp.reset();
			}
		}
	}
	
	private void drawText(GLAutoDrawable drawable) {
		tr.beginRendering(drawable.getWidth(), drawable.getHeight());
		tr.setColor(1f, 1f, 1f, 1f);
		tr.draw("up/down/left/right: change camera | s/S: less/more particle-segments", 10, drawable.getHeight()-20);
		tr.draw("i/I: lower/higher frequency | g/G: less/more particle-generators", 10, drawable.getHeight()-40);
		tr.draw("segments:" + numSegments + ", particles: " + particles.size() + ", generators:" + generators.size() + ", interval:" + particleInterval + "s", 10, 20);
		tr.endRendering();
	}

	public void reshape(GLAutoDrawable drawable, // Window resized
			int x, int y, int width, int height) {
		GL gl0 = drawable.getGL();
		GL2 gl = gl0.getGL2();
		gl.glViewport(0, 0, width, height);
		double aspect = (float) height / width; // aspect-ratio
		bottom = aspect * left;
		top = aspect * right;
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrtho(left, right, bottom, top, near, far); // Viewing-Volume (im Raum)
	}

	public void dispose(GLAutoDrawable drawable) {}

	// --------- Window-Events --------------------

	public void windowClosing(WindowEvent e) {
		System.exit(0);
	}

	public void windowActivated(WindowEvent e) {
	}

	public void windowClosed(WindowEvent e) {
	}

	public void windowDeactivated(WindowEvent e) {
	}

	public void windowDeiconified(WindowEvent e) {
	}

	public void windowIconified(WindowEvent e) {
	}

	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			elev+=1;
			break;
		case KeyEvent.VK_DOWN:
			elev-=1;
			break;
		case KeyEvent.VK_LEFT:
			azim-=1;
			break;
		case KeyEvent.VK_RIGHT:
			azim+=1;
			break;
		case KeyEvent.VK_K:
			if (e.isShiftDown()) {
				if (R<5) R+=0.1;
			} else {
				if (R>0.1) R-=0.1;
			}
			dyn.setR(R);
			resetParticles();
			break;
		case KeyEvent.VK_G:
			if (e.isShiftDown()) {
				synchronized (generators) {
					int listsize = generators.size();
					if (listsize < 100) {
						ParticleGenerator gen = new ParticleGenerator(particles, particleInterval, startX, 0.1 + listsize*0.3 % 7.35, dt, dyn);
						generators.add(gen);
						new Thread(gen).start();
					}
				}
			} else {
				synchronized (generators) {
					if (generators.size()>0) {
						generators.getLast().stop();
						generators.removeLast();
					}
				}
			}
			break;
		case KeyEvent.VK_I:
			if (e.isShiftDown()) {
				if (particleInterval > 0.08) {
					particleInterval-=0.01;	
				}
				
			} else {
				if (particleInterval < 2) {
					particleInterval+=0.01;
				}
			}
			synchronized (generators) {
				for (ParticleGenerator pg : generators) {
					pg.setInterval(particleInterval);
				}
			}
			break;
		case KeyEvent.VK_S:
			if (e.isShiftDown()) {
				if (numSegments<40) numSegments++;
			} else {
				if (numSegments>1) numSegments--;
			}
			break;
		}
		System.out.println("keypressed, elev:" + elev + ", azim:" + azim + ", R:" + R + ", listize:" + generators.size());
	}

	@Override
	public void keyReleased(KeyEvent e) {}

}