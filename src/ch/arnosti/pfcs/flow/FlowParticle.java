package ch.arnosti.pfcs.flow;


import java.util.LinkedList;

import javax.media.opengl.GL2;

import ch.arnosti.pfcs.dynamics.Dynamics;

public class FlowParticle {
	private final double[] origin;
	private final LinkedList<double[]> positions = new LinkedList<>();
	private volatile int stepsrendered = 0;
	private final Dynamics dyn;
	private final double dt;
	private final int spurLength = 5;
	
	public FlowParticle(double x, double y, Dynamics dyn, double dt) {
		origin = new double[]{x,y};
		this.dyn = dyn;
		this.dt = dt;
	}
	
	public void step() {
		step(true);
	}
	
	private void step(boolean inc) {
		if (positions.size() > spurLength) positions.removeFirst();
		positions.addLast(dyn.runge(getPos(), dt));
		if (inc) stepsrendered++;
	}
	
	public void zeichneSpur(GL2 gl) {
		gl.glBegin(GL2.GL_LINE_STRIP);
		gl.glColor3d(0.5, origin[1]/5, origin[1]%1);
		for (double[] p : positions) {
			gl.glVertex3d(0, p[1], -p[0]);
		}
		gl.glEnd();
	}
	
	public double[] getPos() {
		return positions.size() != 0 ? positions.getLast() : origin;
	}
	
	public void reset() {
		positions.clear();
		for (int i=0; i<stepsrendered; i++) {
			step(false);
		}
	}
	
	public int getStepsRendered() {
		return stepsrendered;
	}
}
