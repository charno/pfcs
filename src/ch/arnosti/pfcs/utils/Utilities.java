package ch.arnosti.pfcs.utils;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;

public class Utilities {

	public static void drawBackground(GL2 gl, double height, double width) {
		gl.glClearColor(0.0f, 0.0f, 1.0f, 1.0f); // erasing color
		gl.glClear(gl.GL_COLOR_BUFFER_BIT); // Bild loeschen
		gl.glColor3f(0.3f, 0.3f, 1.0f);
		gl.glBegin(gl.GL_LINES);

		// vertical lines

		for (int i = 0; i < width/2; i++) {
			gl.glVertex2d(1.0 * i, height / 2);
			gl.glVertex2d(1.0 * i, -height / 2);
			gl.glVertex2d(1.0 * -i, height / 2);
			gl.glVertex2d(1.0 * -i, -height / 2);
		}

		// horizontal lines
		for (int i = 0; i < height/2; i++) {
			gl.glVertex2d(width / 2, 1.0 * i);
			gl.glVertex2d(-width / 2, 1.0 * i);
			gl.glVertex2d(width / 2, 1.0 * -i);
			gl.glVertex2d(-width / 2, 1.0 * -i);
		}
		gl.glEnd();
		
		gl.glColor3f(0.6f, 06f, 1.0f);

		gl.glBegin(gl.GL_LINES);
		gl.glVertex2d(-(width/2), 0); // x-Achse
		gl.glVertex2d((width/2), 0);
		gl.glVertex2d(0, -(height/2)); // y-Achse
		gl.glVertex2d(0, (height/2));
		gl.glEnd();

	}

	public static void zeichneAchsen(GL2 gl, double a) // Koordinatenachsen zeichnen
	{
		gl.glColor3d(1, 0, 0);
		gl.glBegin(GL.GL_LINES);
		gl.glVertex3d(0, 0, 0);
		gl.glVertex3d(a, 0, 0);
		gl.glEnd();
		gl.glColor3d(0, 1, 0);
		gl.glBegin(GL.GL_LINES);
		gl.glVertex3d(0, 0, 0);
		gl.glVertex3d(0, a, 0);
		gl.glEnd();
		gl.glColor3d(0, 0, 1);
		gl.glBegin(GL.GL_LINES);
		gl.glVertex3d(0, 0, 0);
		gl.glVertex3d(0, 0, a);
		gl.glEnd();
	}
}
