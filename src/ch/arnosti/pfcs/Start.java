package ch.arnosti.pfcs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;

import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

import ch.arnosti.pfcs.auto.Auto;
import ch.arnosti.pfcs.bouncingball.BouncingBall;
import ch.arnosti.pfcs.flow.Flow;
import ch.arnosti.pfcs.freethrow.FreeThrow;
import ch.arnosti.pfcs.gyroskop.Gyroskop;
import ch.arnosti.pfcs.pendel.Pendel;
import ch.arnosti.pfcs.schieferwurf.SchieferWurf;
import ch.arnosti.pfcs.tennisball.Tennisball;


public class Start extends JFrame {
	
	public Start() {
		this.setSize(300, 300);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		this.setTitle("PFCS: Arnosti Launcher");


		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
		
		JButton pendelbutton = new JButton("Übung 1: Pendel");
		pendelbutton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new Pendel();
			}
		});
		this.add(pendelbutton);
		
		JButton bouncingbutton = new JButton("Unterricht: Bouncing Ball");
		bouncingbutton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new BouncingBall();
			}
		});
		this.add(bouncingbutton);
		
		JButton schieferwurfbutton = new JButton("Unterricht: Schiefer wurf");
		schieferwurfbutton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new SchieferWurf();
			}
		});
		this.add(schieferwurfbutton);
		
		JButton tennisball = new JButton("Übung 2: Tennisball");
		tennisball.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new Tennisball();
			}
		});
		this.add(tennisball);
		
		JButton auto = new JButton("Übung 3: Auto in Kurve");
		auto.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new Auto();
			}
		});
		this.add(auto);
		
		
		JButton gyroskop = new JButton("Übung 4: Gyroskop");
		gyroskop.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new Gyroskop();
			}
		});
		this.add(gyroskop);
		
		
		JButton flow = new JButton("Übung 5: Flow");
		flow.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new Flow();
			}
		});
		this.add(flow);
		
		
		JButton freethrow = new JButton("Übung 6: Frei fliegende Körper");
		freethrow.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new FreeThrow();
			}
		});
		this.add(freethrow);
		
		this.setVisible(true);
	}
	
	public static void main(String... args) {
		new Start();
	}

}
